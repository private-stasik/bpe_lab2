public class C implements F {

    private int c = 42;

    private long e = 1234;

    public long ac() {
        return 111;
    }

    public int[] ii() {
        return new int[]{4, 3, 2, 1};
    }

    public float ff() {
        return 3.14;
    }

    public Object pp() {
        return this;
    }

    public String kk() {
        return "Hello world";
    }

    public int cc() {
        return 42;
    }
}
